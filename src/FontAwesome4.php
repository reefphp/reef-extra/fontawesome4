<?php

namespace ReefExtra\FontAwesome4;

use \Reef\Extension\Extension;

/**
 * Font Awesome 4 icon set
 */
class FontAwesome4 extends Extension {
	
	/**
	 * @inherit
	 */
	public static function getName() : string {
		return 'reef-extra:fontawesome4';
	}
	
	/**
	 * @inherit
	 */
	public static function getDir() : string {
		return __DIR__.'/';
	}
	
	/**
	 * @inherit
	 */
	public function getCSS() : array {
		return [
			[
				'type' => 'local',
				'path' => 'style.css',
				'view' => 'all',
			],
			[
				'type' => 'remote',
				'path' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
				'view' => 'all',
				'name' => 'fontawesome4',
				'integrity' => 'sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN',
			],
		];
	}
	
}
